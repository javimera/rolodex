﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using DatabaseDLL;

namespace RolodexDLL
{
    public class RolodexTabPage : TabPage
    {
        // Private label members
        private Label firstNameLabel;
        private Label lastNameLabel;
        private Label addressLabel;
        private Label cityLabel;
        private Label stateLabel;
        private Label zipLabel;
        private Label homePhoneLabel;
        private Label cellPhoneLabel;

        // Private TextBox members
        private TextBox firstNameBox;
        private TextBox lastNameBox;
        private TextBox addressBox;
        private TextBox cityBox;
        private TextBox stateBox;
        private TextBox zipBox;
        private TextBox homePhoneBox;
        private TextBox cellPhoneBox;
        private Button loadImageButton;
        private Button saveContactButton;
        private PictureBox pictureBox;
        
        // Public class properties

        public Label FirstNameLabel
        {
            get { return this.firstNameLabel; }
        }

        public Label LastNameLabel
        {
            get { return this.lastNameLabel; }
        }

        public TextBox FirstName
        {
            get { return this.firstNameBox; }
        }

        public TextBox LastName
        {
            get { return this.lastNameBox; }
        }

        public TextBox Address
        {
            get { return this.addressBox; }
        }

        public TextBox City
        {
            get { return this.cityBox; }
        }

        public TextBox State
        {
            get { return this.stateBox; }
        }

        public TextBox Zip
        {
            get { return this.zipBox; }
        }

        public TextBox HomePhone
        {
            get { return this.homePhoneBox; }
        }

        public TextBox CellPhone
        {
            get { return this.cellPhoneBox; }
        }

        public PictureBox PictureBox
        {
            get { return this.pictureBox; }
        }

        public Button LoadImageButton
        {
            get { return this.loadImageButton; }
        }

        public Button SaveContactButton
        {
            get { return this.saveContactButton; }
        }

        public RolodexTabPage()
        {
            // Instantiate controls
            this.InstantiateLabels();
            this.InstantiateTextBoxes();
            this.InstantiateImageControls();

            // Create controls
            this.CreateLabels();
            this.CreateTextBoxes();
            this.CreateImageControls();                 
        }

        private void CreateImageControls()
        {
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(224, 187);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(215, 162);            
            this.pictureBox.TabStop = false;
            // 
            // loadImageButton
            // 
            this.loadImageButton.Location = new System.Drawing.Point(15, 274);
            this.loadImageButton.Name = "loadImageButton";
            this.loadImageButton.Size = new System.Drawing.Size(75, 23);
            this.loadImageButton.TabIndex = 9;
            this.loadImageButton.Text = "Load Image";
            this.loadImageButton.UseVisualStyleBackColor = true;

            //
            // saveContactButton
            //
            this.saveContactButton.Location = new Point(15, 310);
            this.saveContactButton.Name = "saveContactButton";
            this.saveContactButton.Size = new Size(100, 40);
            this.saveContactButton.TabIndex = 10;
            this.saveContactButton.Text = "Save Contact";
            this.saveContactButton.UseVisualStyleBackColor = true;
            this.saveContactButton.Visible = false;

            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.loadImageButton);
            this.Controls.Add(this.saveContactButton);
        }

        private void CreateTextBoxes()
        {
            // 
            // firstNameBox
            // 
            this.firstNameBox.Location = new System.Drawing.Point(78, 29);
            this.firstNameBox.Name = "firstNameBox";
            this.firstNameBox.Size = new System.Drawing.Size(118, 20);
            this.firstNameBox.TabIndex = 1;
            // 
            // lastNameBox
            // 
            this.lastNameBox.Location = new System.Drawing.Point(321, 29);
            this.lastNameBox.Name = "lastNameBox";
            this.lastNameBox.Size = new System.Drawing.Size(118, 20);
            this.lastNameBox.TabIndex = 2;
            // 
            // addressBox
            // 
            this.addressBox.Location = new System.Drawing.Point(78, 79);
            this.addressBox.Name = "addressBox";
            this.addressBox.Size = new System.Drawing.Size(361, 20);
            this.addressBox.TabIndex = 3;
            // 
            // cityBox
            // 
            this.cityBox.Location = new System.Drawing.Point(78, 131);
            this.cityBox.Name = "cityBox";
            this.cityBox.Size = new System.Drawing.Size(89, 20);
            this.cityBox.TabIndex = 4;
            // 
            // stateBox
            // 
            this.stateBox.Location = new System.Drawing.Point(244, 131);
            this.stateBox.Name = "stateBox";
            this.stateBox.Size = new System.Drawing.Size(74, 20);
            this.stateBox.TabIndex = 5;
            // 
            // zipBox
            // 
            this.zipBox.Location = new System.Drawing.Point(370, 131);
            this.zipBox.Name = "zipBox";
            this.zipBox.Size = new System.Drawing.Size(69, 20);
            this.zipBox.TabIndex = 6;
            // 
            // homePhoneBox
            // 
            this.homePhoneBox.Location = new System.Drawing.Point(90, 187);
            this.homePhoneBox.Name = "homePhoneBox";
            this.homePhoneBox.Size = new System.Drawing.Size(106, 20);
            this.homePhoneBox.TabIndex = 7;
            // 
            // cellPhoneBox
            // 
            this.cellPhoneBox.Location = new System.Drawing.Point(90, 236);
            this.cellPhoneBox.Name = "cellPhoneBox";
            this.cellPhoneBox.Size = new System.Drawing.Size(106, 20);
            this.cellPhoneBox.TabIndex = 8;       

            this.Controls.Add(this.firstNameBox);
            this.Controls.Add(this.lastNameBox);
            this.Controls.Add(this.addressBox);
            this.Controls.Add(this.cityBox);
            this.Controls.Add(this.stateBox);
            this.Controls.Add(this.zipBox);
            this.Controls.Add(this.homePhoneBox);
            this.Controls.Add(this.cellPhoneBox);
        }

        private void CreateLabels()
        {
            // 
            // firstNameLabel
            // 
            this.firstNameLabel.AutoSize = true;
            this.firstNameLabel.Location = new System.Drawing.Point(12, 32);
            this.firstNameLabel.Name = "firstNameLabel";
            this.firstNameLabel.Size = new System.Drawing.Size(60, 13);            
            this.firstNameLabel.Text = "First Name:";            

            // 
            // lastNameLabel
            // 
            this.lastNameLabel.AutoSize = true;
            this.lastNameLabel.Location = new System.Drawing.Point(255, 32);
            this.lastNameLabel.Name = "lastNameLabel";
            this.lastNameLabel.Size = new System.Drawing.Size(61, 13);            
            this.lastNameLabel.Text = "Last Name:";            
            // 
            // addressLabel
            // 
            this.addressLabel.AutoSize = true;
            this.addressLabel.Location = new System.Drawing.Point(12, 82);
            this.addressLabel.Name = "addressLabel";
            this.addressLabel.Size = new System.Drawing.Size(48, 13);            
            this.addressLabel.Text = "Address:";

            // 
            // cityLabel
            // 
            this.cityLabel.AutoSize = true;
            this.cityLabel.Location = new System.Drawing.Point(12, 134);
            this.cityLabel.Name = "cityLabel";
            this.cityLabel.Size = new System.Drawing.Size(27, 13);
            this.cityLabel.Text = "City:";
            // 
            // stateLabel
            // 
            this.stateLabel.AutoSize = true;
            this.stateLabel.Location = new System.Drawing.Point(196, 134);
            this.stateLabel.Name = "stateLabel";
            this.stateLabel.Size = new System.Drawing.Size(35, 13);            
            this.stateLabel.Text = "State:";
            // 
            // zipLabel
            // 
            this.zipLabel.AutoSize = true;
            this.zipLabel.Location = new System.Drawing.Point(337, 134);
            this.zipLabel.Name = "zipLabel";
            this.zipLabel.Size = new System.Drawing.Size(25, 13);            
            this.zipLabel.Text = "Zip:";
            this.zipLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // homePhoneLabel
            // 
            this.homePhoneLabel.AutoSize = true;
            this.homePhoneLabel.Location = new System.Drawing.Point(12, 190);
            this.homePhoneLabel.Name = "homePhoneLabel";
            this.homePhoneLabel.Size = new System.Drawing.Size(72, 13);            
            this.homePhoneLabel.Text = "Home Phone:";

            // 
            // cellPhoneLabel
            // 
            this.cellPhoneLabel.AutoSize = true;
            this.cellPhoneLabel.Location = new System.Drawing.Point(12, 239);
            this.cellPhoneLabel.Name = "cellPhoneLabel";
            this.cellPhoneLabel.Size = new System.Drawing.Size(61, 13);            
            this.cellPhoneLabel.Text = "Cell Phone:";

            this.Controls.Add(this.firstNameLabel);
            this.Controls.Add(this.lastNameLabel);
            this.Controls.Add(this.addressLabel);
            this.Controls.Add(this.cityLabel);
            this.Controls.Add(this.stateLabel);
            this.Controls.Add(this.zipLabel);
            this.Controls.Add(this.homePhoneLabel);
            this.Controls.Add(this.cellPhoneLabel);
        }

        private void InstantiateLabels()
        {
            this.firstNameLabel = new Label();
            this.lastNameLabel = new Label();
            this.addressLabel = new Label();
            this.cityLabel = new Label();
            this.stateLabel = new Label();
            this.zipLabel = new Label();
            this.homePhoneLabel = new Label();
            this.cellPhoneLabel = new Label();
        }

        private void InstantiateTextBoxes()
        {
            this.firstNameBox = new TextBox();
            this.lastNameBox = new TextBox();
            this.addressBox = new TextBox();
            this.cityBox = new TextBox();
            this.stateBox = new TextBox();
            this.zipBox = new TextBox();
            this.homePhoneBox = new TextBox();
            this.cellPhoneBox = new TextBox();
        }

        private void InstantiateImageControls()
        {
            this.pictureBox = new PictureBox();
            this.loadImageButton = new Button();
            this.saveContactButton = new Button();
            this.loadImageButton.Click +=loadImageButton_Click;
        }

        private void loadImageButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                Bitmap bitmap = new Bitmap(this.pictureBox.Width, this.pictureBox.Height);
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.DrawImage(Image.FromFile(fileDialog.FileName), 0, 0, bitmap.Width, bitmap.Height);

                    this.pictureBox.Image = bitmap;
                }
            }
        }

        public void SetTabControls(bool b)
        {
            foreach (Control c in this.Controls)
            {
                if (c is TextBox)
                {
                    c.Enabled = b;
                }

                if (c is Button)
                {
                    c.Enabled = b;
                }
            }
        }

        public void ShowSerializedRecord(SerializedRecord sr)
        {
            this.FirstName.Text = sr.FirstName;
            this.LastName.Text = sr.LastName;
            this.Address.Text = sr.Address;
            this.City.Text = sr.City;
            this.State.Text = sr.State;
            this.Zip.Text = sr.Zip;
            this.HomePhone.Text = sr.HomePhone;
            this.CellPhone.Text = sr.CellPhone;
            this.PictureBox.Image = sr.Picture;
        }

        public void ClearPage()
        {
            this.FirstName.Text = string.Empty;
            this.LastName.Text = string.Empty;
            this.Address.Text = string.Empty;
            this.City.Text = string.Empty;
            this.State.Text = string.Empty;
            this.Zip.Text = string.Empty;
            this.HomePhone.Text = string.Empty;
            this.CellPhone.Text = string.Empty;
            this.PictureBox.Image = null;
        }

        public SerializedRecord GetRecord()
        {
            SerializedRecord sr = null;

            sr = new SerializedRecord(
                this.FirstName.Text,
                this.LastName.Text,
                this.Address.Text,
                this.City.Text,
                this.State.Text,
                this.Zip.Text,
                this.HomePhone.Text,
                this.CellPhone.Text,
                this.PictureBox.Image);

            return sr;
        }
    }
}
