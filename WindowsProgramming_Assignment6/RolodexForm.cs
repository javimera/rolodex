﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RolodexDLL;
using DatabaseDLL;
using System.IO;

namespace WindowsProgramming_Assignment6
{
    public partial class RolodexForm : Form
    {
        private RolodexTabPage addPage;        
        private RolodexTabPage browsePage;
        private SerializedRecord currentRecord;        
        private Table myTable;

        private bool IsOpenningDB;
        private bool IsOnBrowsesMode;

        public RolodexForm()
        {
            InitializeComponent();            
            this.currentRecord = null;
            this.IsOpenningDB = false;
            this.IsOnBrowsesMode = false;
        }

        private void tabPageControl_Selected(object sender, TabControlEventArgs e)
        {            
            if (e.TabPage != null)
            {                             
                // do not capture the left MouseButton click when opening the database, as it will cause the if statement to execute
                // the if statement is used to capture a tab selection with a mouse press.
                if (!this.IsOpenningDB)
                {
                    if (TabPage.MouseButtons == MouseButtons.Left)
                    {
                        // call the overloaded GetNextRecord method when the user selects a tab by pressing the left button of the mouse
                        this.currentRecord = this.myTable.GetNextRecord(e);                        
                    }                    
                }
                               
                // if the current tab contains a record, then show it on that tab.
                if (this.currentRecord != null)
                {
                    ((RolodexTabPage)e.TabPage).ShowSerializedRecord(this.currentRecord);                    
                }    
                
            }         
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
        private void browseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.IsOnBrowsesMode == false)
                this.IsOnBrowsesMode = !this.IsOnBrowsesMode;

            this.ShowTabs();
        }
       
        private void addEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.IsOnBrowsesMode)
                this.IsOnBrowsesMode = !this.IsOnBrowsesMode;

            this.tabPageControl.TabPages.Clear();

            this.addPage = new RolodexTabPage();
            this.addPage.Text = "Add Contact";
            this.addPage.FirstNameLabel.ForeColor = Color.Red;
            this.addPage.LastNameLabel.ForeColor = Color.Red;
            this.addPage.SaveContactButton.Visible = true;
            this.addPage.SaveContactButton.Click += (s1, e1) =>
            {
                this.SaveContact(this.addPage, sender, e);
            };

            this.tabPageControl.TabPages.Add(this.addPage);
        }
        
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.myTable = new Table();

            if (this.myTable.Open() == 1)
            {                
                this.IsOpenningDB = true;
                
                this.tabPageControl.TabPages.Clear();
                this.SetViewOptions();
                this.ShowTabs();

                this.IsOpenningDB = false;
            }
            else
            {
                this.myTable = null;
            }
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.myTable = new Table();

            if (this.myTable.New() == 1)
            {
                this.tabPageControl.TabPages.Clear();                            
                this.SetViewOptions();
                this.addEntryToolStripMenuItem.Enabled = true;
            }
            else
            {
                this.myTable = null;
            }
        }

        private void firstItemButton_Click(object sender, EventArgs e)
        {
            if (this.myTable != null)
            {
                if (this.IsOnBrowsesMode)
                {
                    // Get first record in the table of records
                    this.currentRecord = this.myTable.GetFirstRecord();

                    if (this.currentRecord != null)
                    {
                        if (this.tabPageControl.SelectedTab.Text == Char.ToUpper(this.currentRecord.LastName[0]).ToString())
                        {
                            ((RolodexTabPage)this.tabPageControl.SelectedTab).ShowSerializedRecord(this.currentRecord);
                        }
                        else
                        {
                            this.tabPageControl.SelectTab(Char.ToUpper(this.currentRecord.LastName[0]) - 65);
                        }
                    }
                }
            }
        }

        private void previousItemButton_Click(object sender, EventArgs e)
        {
            if (this.myTable != null)
            {
                if (this.IsOnBrowsesMode)
                {
                    // get previous record in the table of records
                    this.currentRecord = this.myTable.GetPreviousRecord();

                    // check if there is a previous record in the table of records
                    if (this.currentRecord != null)
                    {
                        if (this.tabPageControl.SelectedTab.Text == Char.ToUpper(this.currentRecord.LastName[0]).ToString())
                        {
                            ((RolodexTabPage)this.tabPageControl.SelectedTab).ShowSerializedRecord(this.currentRecord);
                        }
                        else
                        {
                            this.tabPageControl.SelectTab(Char.ToUpper(this.currentRecord.LastName[0]) - 65);
                        }
                    }
                }
            }
        }

        private void nextItemButton_Click(object sender, EventArgs e)
        {
            if (this.myTable != null)
            {
                if (this.IsOnBrowsesMode)
                {
                    // Get next record in the table of records
                    this.currentRecord = this.myTable.GetNextRecord();

                    if (this.currentRecord != null)
                    {
                        if (this.tabPageControl.SelectedTab.Text == Char.ToUpper(this.currentRecord.LastName[0]).ToString())
                        {
                            ((RolodexTabPage)this.tabPageControl.SelectedTab).ShowSerializedRecord(this.currentRecord);
                        }
                        else
                        {
                            this.tabPageControl.SelectTab(Char.ToUpper(this.currentRecord.LastName[0]) - 65);
                        }
                    }
                }
            }            
        }       

        private void lastItemButton_Click(object sender, EventArgs e)
        {
            if (this.myTable != null)
            {
                if (this.IsOnBrowsesMode)
                {
                    // Get last record in the table
                    this.currentRecord = this.myTable.GetLastRecord();

                    if (this.currentRecord != null)
                    {
                        if (this.tabPageControl.SelectedTab.Text == Char.ToUpper(this.currentRecord.LastName[0]).ToString())
                        {
                            ((RolodexTabPage)this.tabPageControl.SelectedTab).ShowSerializedRecord(this.currentRecord);
                        }
                        else
                        {
                            this.tabPageControl.SelectTab(Char.ToUpper(this.currentRecord.LastName[0]) - 65);
                        }
                    }
                }
            }
        }

        private void SetViewOptions()
        {
            this.addEntryToolStripMenuItem.Enabled = true;
            this.browseToolStripMenuItem.Enabled = true;
            this.deletEntryToolStripMenuItem.Enabled = true;
            this.editEntryToolStripMenuItem.Enabled = true;   
        }

        private void ShowTabs()
        {
            // clear all tab controls when browse item is hit
            this.tabPageControl.TabPages.Clear();
            
            for (int letter = 65; letter <= 90; letter++)
            {
                this.browsePage = new RolodexTabPage();
                this.browsePage.Text = ((char)letter).ToString();
                this.browsePage.SetTabControls(false);
                this.tabPageControl.TabPages.Add(this.browsePage);                
            }

            this.currentRecord = this.myTable.GetFirstRecord();

            if (this.currentRecord != null)
            {
                this.tabPageControl.SelectTab(Char.ToUpper(this.currentRecord.LastName[0]) - 65);
            }
        }

        private void deletEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.myTable.IsTableEmpty)
            {
                if (this.IsOnBrowsesMode)
                {
                    if (MessageBox.Show("Are you sure you want to delete this record?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        ((RolodexTabPage)this.tabPageControl.SelectedTab).ClearPage();

                        this.currentRecord = this.myTable.Delete();

                        if (this.currentRecord != null)
                        {
                            if (this.tabPageControl.SelectedTab.Text == Char.ToUpper(this.currentRecord.LastName[0]).ToString())
                            {
                                ((RolodexTabPage)this.tabPageControl.SelectedTab).ShowSerializedRecord(this.currentRecord);
                            }
                            else
                            {
                                this.tabPageControl.SelectTab(Char.ToUpper(this.currentRecord.LastName[0]) - 65);
                            }
                        }
                        else
                        {
                            ((RolodexTabPage)this.tabPageControl.SelectedTab).ClearPage();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("You can only delete an entry while browsing through the Rolodex.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Address Book is empty.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void editEntryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!this.myTable.IsTableEmpty)
            {
                this.saveChangesToolStripMenuItem.Enabled = true;
                ((RolodexTabPage)this.tabPageControl.SelectedTab).SetTabControls(true);                
            }
            else
            {
                MessageBox.Show("Address Book is empty.", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void saveChangesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SerializedRecord editRecord = ((RolodexTabPage)this.tabPageControl.SelectedTab).GetRecord();

            if (editRecord != null)
            {
                this.myTable.SaveChanges(editRecord);
                ((RolodexTabPage)this.tabPageControl.SelectedTab).SetTabControls(false);
                this.saveChangesToolStripMenuItem.Enabled = false;
            }

            this.saveChangesToolStripMenuItem.Enabled = false;
        }

        private void cLoseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.myTable != null)
            {
                this.DisposeTable();

                this.addEntryToolStripMenuItem.Enabled = false;
                this.deletEntryToolStripMenuItem.Enabled = false;
                this.editEntryToolStripMenuItem.Enabled = false;                
                this.browseToolStripMenuItem.Enabled = false;
            }
            else
            {
                MessageBox.Show("There isn't an address book to close.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DisposeTable()
        {            
            this.myTable.Close();
            this.myTable = null;

            if(this.addPage != null)
                this.addPage.Dispose();

            if(this.browsePage != null)
                this.browsePage.Dispose();

            this.currentRecord = null;
            this.IsOpenningDB = false;
            this.tabPageControl.TabPages.Clear();

        }
        private Image GetEmptyImage()
        {
            Image image = Image.FromFile(Directory.GetCurrentDirectory() + @"\Resources\None.jpg");
            Bitmap bmp = new Bitmap(this.addPage.PictureBox.Width, this.addPage.PictureBox.Height);
            Graphics g = Graphics.FromImage(bmp);
            g.DrawImage(image, 0, 0, bmp.Width, bmp.Height);

            return bmp;
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.ShowDialog();
        }

        private void SaveContact(RolodexTabPage rolodexTabPage, Object sender, EventArgs e)
        {
            if (!(this.addPage.FirstName.Text.Equals(String.Empty)))
            {
                if (!this.addPage.LastName.Text.Equals(String.Empty))
                {
                    Bitmap contactImage;

                    if (this.addPage.PictureBox.Image == null)
                    {
                        contactImage = (Bitmap)this.GetEmptyImage();
                    }
                    else
                    {
                        contactImage = (Bitmap)this.addPage.PictureBox.Image;
                    }

                    SerializedRecord newContact = new SerializedRecord(
                            this.addPage.FirstName.Text,
                            this.addPage.LastName.Text,
                            this.addPage.Address.Text,
                            this.addPage.City.Text,
                            this.addPage.State.Text,
                            this.addPage.Zip.Text,
                            this.addPage.HomePhone.Text,
                            this.addPage.CellPhone.Text,
                            contactImage);

                    int position = this.myTable.RecordAt(newContact);

                    if (position == -1)
                    {
                        this.myTable.Insert(newContact);
                        this.addPage.PictureBox.Image = this.GetEmptyImage();
                        this.addEntryToolStripMenuItem_Click(sender, e);
                    }                             
                }
                else
                {
                    MessageBox.Show("Please fill the last name box.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Please fill the first name box.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}