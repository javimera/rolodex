﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.Windows.Forms;

namespace DatabaseDLL
{
    public class Table
    {
        private List<SerializedRecord> records;
        private BinaryFormatter binaryWriter;
        private BinaryFormatter binaryReader;
        private FileStream fileWriter;
        private FileStream fileReader;
        private string databaseString;
        private string initialDirectory;

        public int CurrentIndex { get; set; }

        public bool IsTableEmpty
        {
            get
            {
                if (this.records.Count == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private bool IsTabClicked;

        public Table()
        {            
            this.binaryReader = null;
            this.binaryWriter = null;
            this.fileReader = null;
            this.fileWriter = null;
            this.databaseString = Directory.GetCurrentDirectory();
            this.initialDirectory = string.Empty;
            this.records = new List<SerializedRecord>();
            this.CurrentIndex = 0;
            this.IsTabClicked = false;
        }        

        public void Insert(SerializedRecord record)
        {
            this.records.Add(record);
            this.records.Sort();
            this.Update();           
        }        

        public int Open()
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.InitialDirectory = this.initialDirectory;
            fileDialog.Filter = "Database File (*.abd)|*.abd";

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                if (File.Exists(fileDialog.FileName))
                {                    
                    using (this.fileReader = File.Open(fileDialog.FileName, FileMode.Open))
                    {
                        if (this.fileReader.Length > 0)
                        {
                            this.binaryReader = new BinaryFormatter();
                            this.records = (List<SerializedRecord>)this.binaryReader.Deserialize(this.fileReader);                            
                            this.fileReader.Close();
                            this.binaryReader = null;
                        }                        

                        this.databaseString = fileDialog.FileName;

                        return 1; // return 1 if the database was opened successfully.
                    }
                }
                else
                {
                    return 0; // return 0 if the database was not opened.
                }            
            }

            return 0; // return 0 if the database was not opened.
        }

        public int New()
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            saveFile.InitialDirectory = this.initialDirectory;
            saveFile.Filter = "Database File (*.abd)|*.abd";

            if (saveFile.ShowDialog() == DialogResult.OK)
            {
                using (this.fileWriter = File.Open(saveFile.FileName, FileMode.Create))
                {
                    this.databaseString = saveFile.FileName;
                    this.fileWriter.Close();

                    return 1; // return 1 if the database was opened successfully.
                }
            }

            return 0;
        }
        
        public void Close()
        {
            // check if the table cointains any records before trying to dispose it.
            if(this.records.Count > 0)
                this.records.RemoveRange(0, this.records.Count - 1);

            this.binaryReader = null;
            this.binaryWriter = null;
            this.fileReader = null;
            this.fileWriter = null;
            this.databaseString = string.Empty;
            this.initialDirectory = null;                        
        }             
        
        public SerializedRecord GetFirstRecord()
        {
            if (this.records.Count > 0)
            {
                this.CurrentIndex = 0;
                return this.records[this.CurrentIndex];
            }
            else
            {
                return null;
            }
        }

        public SerializedRecord GetNextRecord()
        {            
            if (this.records.Count > 0)
            {
                if (this.CurrentIndex >= this.records.Count - 1)
                {
                    return this.GetLastRecord();
                }                
                else
                {
                    if (this.IsTabClicked)
                    {
                        // Dont increment the index if a tab has been selected by a mouse click, as this will cause the index to be on the next record.
                        // If the index were to be incremented after a tab selection by a mouse click, then the next record would be skipped if the user pressed the next button
                        this.IsTabClicked = false;
                    }
                    else
                    {
                        // if a tab hasn't been selected by a mouseclick, but by the next button, get the next record
                        this.CurrentIndex++;
                    }
                                        
                    return this.records[this.CurrentIndex];
                }
            }
            else
            {                
                return null;
            }
        }

        public SerializedRecord GetNextRecord(TabControlEventArgs tab)
        {
            SerializedRecord sr = null;

            // check if there is a record on the tab selected with a mouse click
            sr = this.records.FirstOrDefault(r => Char.ToUpper(r.LastName[0]).ToString() == tab.TabPage.Text);

            if (sr != null)
            {
                this.IsTabClicked = false;
                this.CurrentIndex = this.records.IndexOf(sr);

                return sr;
            }
            else
            {                
                // if there is a record, then set currentIndex back to 0 so it can count how many records are behind the tab that the user clicked
                this.CurrentIndex = 0;

                // set to true to indicate a tab has been selected with a mouse click
                this.IsTabClicked = true;

                for (int letter = 0; letter <= tab.TabPageIndex; letter++)
                {                                
                    // use firstOrDefault to only retrieve the first record from each tab (if there are any)
                    List<SerializedRecord> srList = this.records.FindAll(r => Char.ToUpper(r.LastName[0]).ToString() == Convert.ToChar(letter+65).ToString());

                    if (srList != null)
                    {                      
                        // only increment index if there is only 1 record per tab that is behind the tab selected by the user
                        this.CurrentIndex += srList.Count;                     
                    }
                }

                return null;
            }            
        }

        public SerializedRecord GetPreviousRecord()
        {
            // if a tab has been selected by a mouse click, and the user decides to get the previous record, only set the variable to false if there are records 
            // before the selected tab.
            // If the user selected a tab that has no records before it, the nsetting it to false would cause the next record to be skipped when pressing the next button.
            if (this.IsTabClicked && this.CurrentIndex > 0)
            {
                this.IsTabClicked = false;
            }

            // check if the list of records contains at least 1 record to return.
            if (this.records.Count > 0)
            {                
                SerializedRecord sr = null;

                // check if the index is able to return a record, else it is sitting on position 0
                if (this.CurrentIndex > 0)
                {                    
                    this.CurrentIndex--;
                    sr = this.records[this.CurrentIndex];
                }
                else if (this.CurrentIndex == 0)
                {                    
                    return null;
                }                
                
                return sr;
            }            
            else
            {
                // return a null record if there are no previous records to return
                return null;
            }
        }        

        public SerializedRecord GetLastRecord()
        {
            if (this.records.Count > 0)
            {                
                this.CurrentIndex = this.records.Count - 1;
                return this.records[this.CurrentIndex];
            }
            else
            {
                return null;
            }
        }

        public int RecordAt(SerializedRecord sr)
        {
            return this.records.IndexOf(sr);
        }

        private void Update()
        {
            using (this.fileWriter = File.Open(this.databaseString, FileMode.Create))
            {
                this.binaryWriter = new BinaryFormatter();
                this.binaryWriter.Serialize(this.fileWriter, this.records);
                this.fileWriter.Close();
            }

            this.records.Sort();
        }

        public SerializedRecord Delete()
        {
            if (this.records.Count > 0 && this.CurrentIndex > -1)
            {
                this.records.RemoveAt(this.CurrentIndex);
                this.Update();

                if (this.records.Count > 0)
                {
                    return this.GetFirstRecord();
                }
                else if(this.records.Count == 0)
                {
                    this.CurrentIndex = 0;
                    return null;
                }
            }

            return null;
        }

        public void SaveChanges(int position, SerializedRecord sr)
        {
            this.records[position] = sr;
            this.Update();
        }

        public void SaveChanges(SerializedRecord sr)
        {
            this.records[this.CurrentIndex] = sr;
            this.Update();
        }        
    }
}
