﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace DatabaseDLL
{
    [Serializable]
    public class SerializedRecord : IComparable
    {
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Address { get; private set; }
        public string City { get; private set; }
        public string State { get; private set; }
        public string Zip { get; private set; }
        public string HomePhone { get; private set; }
        public string CellPhone { get; private set; }
        public Image Picture { get; private set; }

        public SerializedRecord(string firstname, string lastname, string address, string city, string state, string zip, string homenumber, string cellnumber, Image bmp)
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Address = address;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.HomePhone = homenumber;
            this.CellPhone = cellnumber;
            this.Picture = bmp;
        }        
        
        int IComparable.CompareTo(object obj)
        {
            SerializedRecord tempRecord = obj as SerializedRecord;

            if (tempRecord != null)
            {
                if (this.LastName.CompareTo(tempRecord.LastName) == 0)
                {
                    return this.FirstName.CompareTo(tempRecord.FirstName);
                }
                else
                {
                    return this.LastName.CompareTo(tempRecord.LastName);
                }
            }
            else
                throw new ArgumentException("Parameter is not a Serialized Record");
        }
    }   
}
